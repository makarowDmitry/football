package ru.mda.football;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Integer scoreD = 0, scoreS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView counterLeft = (TextView) findViewById(R.id.scoreDeut);
        TextView counterRight = (TextView) findViewById(R.id.scoreSpain);
        if (savedInstanceState != null) {
            counterLeft.setText(savedInstanceState.getInt("Deut") + "");
            counterRight.setText(savedInstanceState.getInt("Spain") + "");
        }

    }

    public void onClickButtonDeuschland(View view) {
        scoreD++;
        TextView counterLeft = (TextView) findViewById(R.id.scoreDeut);
        counterLeft.setText(scoreD + "");
    }

    public void onClickButtonSpain(View view) {
        scoreS++;
        TextView counterRight = (TextView) findViewById(R.id.scoreSpain);
        counterRight.setText(scoreS + "");
    }

    public void onClickButtonRestart(View view) {
        scoreS = 0;
        scoreD = 0;
        TextView counterLeft = (TextView) findViewById(R.id.scoreDeut);
        TextView counterRight = (TextView) findViewById(R.id.scoreSpain);
        counterLeft.setText(scoreD + "");
        counterRight.setText(scoreS + "");
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        scoreD = savedInstanceState.getInt("Deut");
        scoreS = savedInstanceState.getInt("Spain");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("Deut", scoreD);
        outState.putInt("Spain", scoreS);
    }

}
